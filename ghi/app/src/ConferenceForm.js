import { useEffect, useState } from 'react';

function ConferenceForm () {

    const [name, setName] = useState("");
    const [starts, setStart] = useState("");
    const [ends, setEnd] = useState("");
    const [desc, setDesc] = useState("");
    const [attendees, setAttendees] = useState("");
    const [presentations, setPresentations] = useState("");
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState("");

    const handleNameChange = e => {
        const value = e.target.value;
        setName(value);
    }
    const handleStartChange = e => {
        const value = e.target.value;
        setStart(value);
    }
    const handleEndChange = e => {
        const value = e.target.value;
        setEnd(value);
    }
    const handleDescChange = e => {
        const value = e.target.value;
        setDesc(value);
    }
    const handleAttendeesChange = e => {
        const value = e.target.value;
        setAttendees(value);
    }
    const handlePresentationChange = e => {
        const value = e.target.value;
        setPresentations(value);
    }
    const handleLocationChange = e => {
        const value = e.target.value;
        setLocation(value);
    }

    const fetchData = async () => {

        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
          }
        }

      useEffect(() => {
        fetchData();
      }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = desc;
        data.max_presentations = presentations;
        data.max_attendees = attendees;
        data.location = location;

        const url = 'http://localhost:8000/api/conferences/';
        const json = JSON.stringify(data);

        const fetchConfig = {
            method: "POST",
            body: json,
            headers: {
            'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStart('');
            setEnd('');
            setDesc('');
            setLocation('');
            setAttendees('');
            setPresentations('');
        }
        }

    return(
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
                <input value={starts} onChange={handleStartChange} placeholder="Start date" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="start">Starts</label>
            </div>
            <div className="form-floating mb-3">
                <input value={ends} onChange={handleEndChange} placeholder="End date" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
            </div>
            <div className="form-floating mb-3">
                <input value={presentations} onChange={handlePresentationChange} placeholder="amount of presentations" required type="text" name="presentations" id="presentations" className="form-control"/>
                <label htmlFor="presentations">Max Presentations</label>
            </div>
            <div className="form-floating mb-3">
                <input value={attendees} onChange={handleAttendeesChange} placeholder="Max attendees" required type="text" name="attendees" id="attendees" className="form-control"/>
                <label htmlFor="attendees">Attendees</label>
            </div>
            <div className="form-floating mb-3">
                <textarea value={desc} onChange={handleDescChange} placeholder="description" required type="text" name="desc" id="desc" className="form-control"/>
                <label htmlFor="desc">Description</label>
            </div>
            <div className="mb-3">
            <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                    {locations.map((location) => {
                        return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                        );
                    })}
            </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
  </div>
)}

export default ConferenceForm;
